# Brief description
An IE&#45;friendly drag drop&#45;&#45; for edge&#45;cases requiring versions 8 or less specifically &#45;&#45;which demonstrates feasibility with drag drop as text in terms of javascript mechanics.

## Please visit
You can see [the project](https://artavia.gitlab.io/drag_drop "the project at github") for yourself and decide whether it is really for you or not. 

## A lesson to those with loose lips
This exhibit is fluff. It&rsquo;s only purpose is to make you smile. It does nothing other than break expectations. When somebody says that something cannot be done it only makes me curious to know the motivation behind such a careless statement; in this case somebody previously stated that drag and drop cannot be done in IE8 or less and I beg to differ with all due respect.

### Kudos
This is a refax of [one of several](http://jsbin.com/ijeza/510 "link to an example at jsbin") exercises that the [html dokutas](http://html5doctor.com/native-drag-and-drop/ "link to drag and drop lesson at html5doctor") posted at the time. I can objectively state that their enthusiasm for the subject helped bring clarity. I thank you. 
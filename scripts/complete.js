var DRYOBJ = ( function ( ) {
	
	/*################  ES5 pragma  ######################*/
	'use strict';	

	function _RetValFalprevDef( leEvt ) { 
		if( typeof leEvt !== "undefined") {
			var _EREF = leEvt; 
		} 
		else {
			var _EREF = window.event; 
		}		
		if( _EREF.preventDefault) {
			_EREF.preventDefault(); 
		}
		else {
			_EREF.returnValue = false; 
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== "undefined") { 
			var _EREF = leEvt; 
		}
		else {
			var _EREF = window.event; 
		}
		if( typeof _EREF.target !== "undefined") {
			var evtTrgt = _EREF.target;	
		}
		else {
			var evtTrgt = _EREF.srcElement; 
		}
		return evtTrgt;
	}

	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	return {
		Utils : {
			evt_u : {
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				}	, 
				RetValFalprevDef : function( leEvt ) { 
					return _RetValFalprevDef( leEvt );
				} 
			} 
		}
			
	}; // END public properties
	
}( ) ); // console.log( DRYOBJ ); 



var Gh_pages = ( function() {	
	
	/*################  ES5 pragma  ######################*/
	'use strict';

	var _docObj = window.document;
	var allImgs = _docObj.getElementsByTagName( "img" ), _Z;
	var imgObj;
	var ar_DZs = [], allDivs = _docObj.getElementsByTagName( "div" );
	var evtTrgt, data, _A, _B, _C, _D;
	var dndMax = null;
	
	function _AllowF(leEvt){ 
		_C = 0;
		for( _C; _C < dndMax; _C = _C + 1 ){
			if( DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).nodeName.toLowerCase() === "div" && ar_DZs[ _C ].id === DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id ) {
				DRYOBJ.Utils.evt_u.RetValFalprevDef(leEvt);  
			} 
		}
	}
	
	function _DragF(leEvt) { 
		leEvt.dataTransfer.setData( "Text", DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id );
	}
	
	function _DropF(leEvt) { 
		_D = 0;
		for( _D; _D < dndMax; _D = _D + 1 ){
			if( DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).nodeName.toLowerCase() === "div" && ar_DZs[ _D ].id === DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt).id ) { 
				DRYOBJ.Utils.evt_u.RetValFalprevDef(leEvt); 
				evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget(leEvt);
				data = leEvt.dataTransfer.getData( "Text" );	
				evtTrgt.appendChild( document.getElementById( data ) );
			}
		}
	}

	function _DOMCONLO() {
		_A = 0, _B = 0; 
		_Z = 0;
		
		for (_Z; _Z < allImgs.length; _Z = _Z + 1 ) {
			if( allImgs[ _Z ].className === "back_n_forth" ) { 
				imgObj = allImgs[ _Z ];
				imgObj.draggable = true; 
				DRYOBJ.Utils.evt_u.AddEventoHandler( imgObj, "dragstart", _DragF ); 
			}
		}
		
		for (_A; _A < allDivs.length; _A = _A + 1 ) {
			if( allDivs[ _A ].className === "dz" ) { 
				ar_DZs.push( allDivs[ _A ] );
				dndMax = ar_DZs.length;
			}
		} 
		
		for( _B; _B < dndMax; _B = _B + 1 ){ 
			DRYOBJ.Utils.evt_u.AddEventoHandler( ar_DZs[ _B ], "drop", _DropF ); 
			DRYOBJ.Utils.evt_u.AddEventoHandler( ar_DZs[ _B ], "dragover", _AllowF ); 
			if(_docObj.documentElement.attachEvent) { 
				DRYOBJ.Utils.evt_u.AddEventoHandler( ar_DZs[ _B ], "dragenter", _AllowF ); 
			}
		} 
		
	}
	
	// END of _private properties
	return {
		InitDCL: function() {
			return _DOMCONLO(); 
		} 
	};
} )(); // window.Gh_pages

DRYOBJ.Utils.evt_u.AddEventoHandler( window , "DOMContentLoaded" , Gh_pages.InitDCL() ); 